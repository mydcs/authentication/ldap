<a name="unreleased"></a>
## [Unreleased]


<a name="0.0.3"></a>
## [0.0.3] - 2023-04-07
### Chore
- CHANGELOG 0.0.3
- rebase
- no debugging

### Cicd
- Add multiple arch for latest and tagged version
- multiple architecture deployment

### Doc
- wrong place for config
- Config for CHANGELOG


<a name="0.0.2"></a>
## [0.0.2] - 2023-04-01
### Chore
- CHANGELOG 0.0.1

### Cicd
- pipeline failed

### Fix
- Change default domain to testing domain mydcs.dev

### Testing
- Added script for local testing


<a name="0.0.1"></a>
## 0.0.1 - 2023-02-17
### Feat
- first release


[Unreleased]: https://gitlab.com/mydcs/devops/compare/0.0.3...HEAD
[0.0.3]: https://gitlab.com/mydcs/devops/compare/0.0.2...0.0.3
[0.0.2]: https://gitlab.com/mydcs/devops/compare/0.0.1...0.0.2

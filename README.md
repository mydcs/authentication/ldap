# Status

[gitlab-pipeline-image]: https://gitlab.com/mydcs/authentication/ldap/badges/main/pipeline.svg
[gitlab-pipeline-link]: https://gitlab.com/mydcs/authentication/ldap/-/commits/main
[gitlab-release-image]: https://gitlab.com/mydcs/authentication/ldap/-/badges/release.svg
[gitlab-release-link]: https://gitlab.com/mydcs/authentication/ldap/-/releases
[gitlab-stars-image]: https://img.shields.io/gitlab/stars/mydcs/authentication/ldap?gitlab_url=https%3A%2F%2Fgitlab.com
[gitlab-stars-link]: https://hub.docker.com/r/mydcs/authentication/ldap

[docker-pull-image]: https://img.shields.io/docker/pulls/mydcs/ldap.svg
[docker-pull-link]: https://hub.docker.com/r/mydcs/ldap
[docker-release-image]: https://img.shields.io/docker/v/mydcs/ldap?sort=semver
[docker-release-link]: https://hub.docker.com/r/mydcs/ldap
[docker-stars-image]: https://img.shields.io/docker/stars/mydcs/ldap.svg
[docker-stars-link]: https://hub.docker.com/r/mydcs/ldap
[docker-size-image]: https://img.shields.io/docker/image-size/mydcs/ldap/latest.svg
[docker-size-link]: https://hub.docker.com/r/mydcs/ldap


[![gitlab-pipeline-image]][gitlab-pipeline-link] 
[![gitlab-release-image]][gitlab-release-link]
[![gitlab-stars-image]][gitlab-stars-link]


[![docker-pull-image]][docker-pull-link] 
[![docker-release-image]][docker-release-link]
[![docker-stars-image]][docker-stars-link]
[![docker-size-image]][docker-size-link]

# How To

## Vorbereitung

1. Arbeitsverzeichnis anlegen
2. In das Arbeitsverzeichnis wechseln
3. Unterordner config anlegen und die Beispiel Datei dort ablegen
4. Beispiel-Datei nach eigenen Bedürfnissen anpassen
5. Container starten, entweder ueber "docker run" oder "docker compose"

## Aufruf

### CLI

```
docker run -it --rm -e DOMAIN=example -e TLD=com -v $(pwd)/config:/srv -v /etc/letsencrypt:/etc/letsencrypt mydcs/ldap:latest
```

### Compose

```dockerfile
version: "3.8"

services:
  ldap:
    container_name: ldap
    hostname: ldap
    restart: always
    build:
      context: ./ldap
      args:
        DIST: alpine
        REL: 3.17
    environment:
      - DOMAIN=${DOMAIN:-example}
      - TLD=${TLD:-com}
      - LDAPDEBUG=${LDAPDEBUG:-none}
      - LDAPROOT_CN=${LDAPROOT_CN:-admin}
      - ADMIN_PASSWORD=${ADMIN_PASSWORD:-secret}
    networks:
      - openldap
    ports:
      - 389:389
      - 636:636
    volumes:
      - /srv/storage/IaaS/${ENVIRONMENT:-dev}/authentication/ldap:/srv
      - /srv/storage/IaaS/${ENVIRONMENT:-dev}/ssl:/etc/ssl/letsencrypt

networks:
  openldap:
    name: iaas
```

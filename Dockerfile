FROM	alpine:3.17
LABEL	maintainer=Dis4sterRec0very

#
# Setup environment
#
ENV	DOMAIN=${DOMAIN:-mydcs}
ENV	TLD=${TLD:-dev}
ENV	ADMIN_PASSWORD=${ADMIN_PASSWORD:-secret}
ENV	LDAPDEBUG=none
ENV 	LDAPURI="ldapi:/// ldap:///"

#
# Install OpenLDAP and arrange directory structure
#
RUN	apk --no-cache --update add \
	htop \
	openldap \
	openldap-backend-all \
	openldap-overlay-all \
	openldap-clients \
	openldap-passwd-sha2 

#
# Copy utility scripts including docker-entrypoint.sh to image
#
COPY	img_fs /

HEALTHCHECK CMD whoami || exit 1

ENTRYPOINT ["./entrypoint.sh"]

CMD	[]

EXPOSE	389/tcp 636/tcp

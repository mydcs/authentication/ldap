#!/bin/sh

export DOMAIN=${DOMAIN:-mydcs}
export TLD=${TLD:-dev}
export LDAPDEBUG=${LDAPDEBUG:-none}
export ADMIN_PASSWORD=${ADMIN_PASSWORD:-secret}
export SSLFILE=/etc/ssl/letsencrypt/fullchain.pem

if [ ! -d /srv/$DOMAIN.$TLD/config ]; then
	echo "BASE dc=$DOMAIN,dc=$TLD" > /etc/openldap/ldap.conf
	if [ -f $SSLFILE ]; then
		echo "URI ldaps://127.0.0.1:636" >> /etc/openldap/ldap.conf
		echo "TLS_CACERT $SSLFILE" >> /etc/openldap/ldap.conf
	else
		echo "URI ldap://127.0.0.1:389" >> /etc/openldap/ldap.conf
		sed -i "s/^olcTLS/#olcTLS/g" /data/000_init.ldif
	fi

	echo "Init LDAP database"
	install -m 755 -o ldap -g ldap -d /srv/$DOMAIN.$TLD/config
	install -m 700 -o ldap -g ldap -d /srv/$DOMAIN.$TLD/data
	sed -i "s#^olcSuffix: .*#olcSuffix: dc=$DOMAIN,dc=$TLD#g" /data/000_init.ldif
	sed -i "s#^olcRootDN: .*#olcRootDN: cn=Manager,dc=$DOMAIN,dc=$TLD#g" /data/000_init.ldif
	sed -i "s#^olcRootPW: .*#olcRootPW: $ADMIN_PASSWORD#g" /data/000_init.ldif
	sed -i "s#^olcDbDirectory:.*#olcDbDirectory: /srv/$DOMAIN.$TLD/data#g" /data/000_init.ldif
	# add basic data
	echo "Add basic groups"
	sed -i "s#dc=mydcs,dc=dev#dc=$DOMAIN,dc=$TLD#g" /data/*.ldif
	sed	-i "s#mydcs.dev#$DOMAIN.$TLD#g" /data/*.ldif
	sed -i "s#mydcs#$DOMAIN#g" /data/*.ldif
	for ldif in $(ls -1 /data/*.ldif)
	do
		echo "Process $ldif"
		DB=1
		if [ "$(basename $ldif .ldif)" = "000_init" ]; then
			DB=0
		fi
		slapadd -n $DB -F /srv/$DOMAIN.$TLD/config -l $ldif
		if [ $? -eq 0 ]; then
			mv $ldif $ldif.ok
		else
			mv $ldif $ldif.err
		fi
	done
fi

# add external data
if [ -f /srv/[0-9][0-9][0-9]_*.ldif ]; then
	for ldif in $(ls -1 /srv/[0-9][0-9][0-9]_*.ldif)
	do
		echo "Process $ldif"
		slapadd -n 1 -F /srv/$DOMAIN.$TLD/config -l $ldif
		if [ $? -eq 0 ]; then
			mv $ldif $ldif.ok
		else
			mv $ldif $ldif.err
		fi
	done
fi

ulimit -n 1024
if [ -f $SSLFILE ]; then
	slapd -d ${LDAPDEBUG} -F /srv/$DOMAIN.$TLD/config -h "ldap:/// ldaps:///"
else
	slapd -d ${LDAPDEBUG} -F /srv/$DOMAIN.$TLD/config -h "ldap:///"
fi
